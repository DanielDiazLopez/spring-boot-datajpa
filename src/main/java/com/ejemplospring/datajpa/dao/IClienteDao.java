package com.ejemplospring.datajpa.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ejemplospring.datajpa.models.entity.Cliente;

public interface IClienteDao extends PagingAndSortingRepository<Cliente, Long> {


}
